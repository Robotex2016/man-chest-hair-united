import cv2
import numpy as np
import math

import serial
from time import sleep


###     OLEKUMASIN     ###
import sys

END = 0
FIND_BALL = 1
FIND_GOAL = 2
HIT_GOAL = 8

pal_asukoht = ""
PORT = 'COM3'
CAMERA = 1
BUFFER = 3


###     FUNCTIONS     ###


# drive to ball
def drive_to_ball():
    #if ball_in_dribbler():
    #    turn('stop')
    #    STATE = FIND_GOAL
    #else:
        hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
        hsv = cv2.GaussianBlur(hsv, (25, 25), 0)

        lowerBalls = np.array([hlBalls, slBalls, vlBalls])
        upperBalls = np.array([huBalls, suBalls, vuBalls])

        findBalls(hsv, lowerBalls, upperBalls)

        global Ballx, Bally, STATE
        calculateAngle(Ballx, Bally)


def calculateAngle(ballx, bally):
    x = 320 - ballx # vasakul positiivne
    y = 480 - bally # 400 umbes palli koht dribbleris

    z = math.sqrt(x*x + y*y)

    suhe = y / z
    if (x < 0):
        #print -suhe, "   ", z
        drive(-suhe)
    else:
        #print suhe, "   ", z
        drive(suhe)

def drive(suhe):
    def_speed = 40
    max_speed = 45

    abs_angle = abs(suhe)

    speed_left = def_speed
    speed_right = int(speed_left/abs_angle)

    #max v6imsuse kontroll
    if (speed_right > max_speed):
        speed_left = (speed_left * max_speed) / speed_right
        speed_right = max_speed

    if (suhe < 0): # vahetan
        muutuja = speed_right
        speed_right = speed_left
        speed_left = muutuja

    string = "<2wl" + str(speed_right) + ";3wl" + str(speed_left) + ">\n"
    #print(string_left +"        "+ string_right)

    port.write(string)
    sleep(0.05)

def turn(s):
    if (s == 'right'):
        port.write("<1wl25;2wl25;3wl-25>\n")
        print("right")
    elif (s == 'left'):
        port.write("<1wl-25;2wl-25;3wl25>\n")
        print("left")
    elif (s == 'stop'):
        port.write("<1wl0;2wl0;3wl0>\n")
        print("stop")


def dribbler(activate):
    if activate:

        port.write("<0do54>\n")
    else:
        port.write("<0do51>\n")

def init_dribbler():
    port.write("<0do70>\n")
    sleep(3)
    port.write("<0do10>\n")
    sleep(2)

def ball_in_dribbler():
    port.write("<0bd>\n")
    sleep(0.05)
    try:
        n = port.readline()
        if (n[0] == '1'):
           return True
        else:
            return False
    except Exception, e:
        print "Serial port error: " + str(e)





#find and align with the goal
def find_goal():
    if ball_in_dribbler():
        hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
        hsv = cv2.GaussianBlur(hsv, (25, 25), 0)

        lowerGoal = np.array([hlGoal, slGoal, vlGoal])
        upperGoal = np.array([huGoal, suGoal, vuGoal])

        findGoal(hsv, lowerGoal, upperGoal)

        if Goalx > 350:
            print("left")
            # p66ra paremale
            port.write("<1wl-50>\n")
        elif Goalx < 290:
            # p66ra vasakule
            print("right")
            port.write("<1wl50>\n")
        else:
            # kohal
            print("GOAL ALIGNED")
            port.write("<1wl0>\n")
            global STATE
            STATE = HIT_GOAL
            return
    else:
        STATE = FIND_BALL



def hit_goal():
    port.write("COILGUN STUFF")
    print "GOAL SCORED?!? \n"
    global STATE
    STATE = END


def end():
    print "ENDING"
    global STATE
    STATE = END







def findBalls(hsvBalls, lower, upper):
    mask = cv2.inRange(hsvBalls, lower, upper)
    global erodeBalls, Ballx, Bally, Ballradius
    Ballradius = 0
    erodeBalls = cv2.erode(mask, None, iterations=2)
    mask_contours, contours, hierarchy = cv2.findContours(erodeBalls.copy(), cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

    if len(contours) > 0:
        try:
            # leian suurima kontuuri
            contoursMax = max(contours, key=cv2.contourArea)
            ((Ballx, Bally), Ballradius) = cv2.minEnclosingCircle(contoursMax)
            # circle: image, center, radius, color, thickness
            cv2.circle(frame, (int(Ballx), int(Bally)), int(Ballradius), (0, 255, 0), 2)
            cv2.circle(frame, (int(Ballx), int(Bally)), 2, (0, 0, 255), 5)
        except:
            e = sys.exc_info()[0]
            print("findballs ERROR")
            print(e)


def findGoal(hsvGoal, lower, upper):
    mask = cv2.inRange(hsvGoal, lower, upper)
    global erodeGoal, Goalx
    erodeGoal = cv2.erode(mask, None, iterations=2)
    mask_contours, contours, hierarchy = cv2.findContours(erodeGoal.copy(), cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

    if len(contours) > 0:
        try:
            # cv2.drawContours(frame, contours, -1, (255, 0, 0), 3)
            contoursMax = max(contours, key=cv2.contourArea)
            cv2.drawContours(frame, contoursMax, -1, (255, 105, 180), 3)
            M = cv2.moments(contoursMax)
            try:
                cx = int(M['m10'] / M['m00'])
                cy = int(M['m01'] / M['m00'])
                cv2.circle(frame, (int(cx), int(cy)), 2, (0, 0, 233), 5)
                Goalx = cx
            except:
                print('OMG NO GOALS')
        except:
            e = sys.exc_info()[0]
            print("findballs ERROR")
            print(e)

def writefile(filename, hl, sl, vl, hu, su, vu):
    file = open(filename, 'w')
    file.write(str(hl) + "\n" + str(sl) + "\n" + str(vl) + "\n" +
               str(hu) + "\n" + str(su) + "\n" + str(vu) + "\n")
    file.close()


def readfile(filename):
    try:
        file = open(filename, 'r')
        hl = int(file.readline())
        sl = int(file.readline())
        vl = int(file.readline())
        hu = int(file.readline())
        su = int(file.readline())
        vu = int(file.readline())
        file.close()

        return hl, sl, vl, hu, su, vu

    except IOError:
        writefile(filename, 0, 0, 0, 0, 0, 0)
        return 0, 0, 0, 0, 0, 0



###     DECLARATION, GLOBAL VARIABLES    ###
hlBalls, slBalls, vlBalls, huBalls, suBalls, vuBalls = readfile('ball_values.txt')
hlGoal, slGoal, vlGoal, huGoal, suGoal, vuGoal = readfile('goal_values.txt')


cap = cv2.VideoCapture(CAMERA)
_, erodeGoal = cap.read()  # v22rtustamise jaoks, (muutub findBalls ja findGoal funktsioonides)
erodeBalls = erodeGoal
sleep(0.3)

Ballx = 0.0
Bally = 0.0
Ballradius = 0.0
Goalx = 0.0

port = serial.Serial(PORT, baudrate=9600, timeout=None)
print("connected to: " + port.portstr)



#init_dribbler()
#dribbler(True)


# olekumasin mapping
states = { END: end,
           FIND_BALL: drive_to_ball,
           FIND_GOAL: find_goal,
           HIT_GOAL: hit_goal
}

STATE = FIND_BALL


###    MAIN LOOP    ###
while(STATE != 0):
    # frame buffer
    for i in range(BUFFER):
        ret, frame = cap.read()

    #states[STATE]()

    # frame 640x480
    cv2.imshow('frame', frame)

    k = cv2.waitKey(5) & 0xFF

    if k == ord('c'):
        if ball_in_dribbler():
            print "Pall on sees"
        else:
            print "Pall ei ole see"

    if k == 27:
        dribbler(False)
        cap.release()
        port.write("<1wl0;2wl0;3wl0;0do0>\n")
        sleep(0.05)
        port.close()
        break


cv2.destroyAllWindows()

