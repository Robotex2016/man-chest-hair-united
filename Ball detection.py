import cv2
import numpy as np
import math


CAMERA = 1
BUFFER = 1

###     FUNCTIONS     ###

def calculateAngle(ballx, bally):
    x = 320 - ballx # vasakul positiivne
    y = 400 - bally # 400 umbes palli koht dribbleris

    z = math.sqrt(x*x + y*y)

    suhe = y / z
    if (x < 0):
        #print -suhe, "   ", z
        drive(-suhe)
    else:
        #print suhe, "   ", z
        drive(suhe)

def drive(suhe):
    def_speed = 30
    max_speed = 50

    abs_angle = abs(suhe)

    speed_left = def_speed
    speed_right = int(speed_left/abs_angle)

    #max v6imsuse kontroll
    if (speed_right > max_speed):
        speed_left = (speed_left * max_speed) / speed_right
        speed_right = max_speed

    if (suhe < 0): # vahetan
        muutuja = speed_right
        speed_right = speed_left
        speed_left = muutuja


    string_left = "2wl-" + str(speed_left) + ""
    string_right = "3wl-" + str(speed_right) + "\n"
    #print(string_left +"        "+ string_right)


def findGoal(hsvGoal, lower, upper):
    mask = cv2.inRange(hsvGoal, lower, upper)
    global erodeGoal
    erodeGoal = cv2.erode(mask, None, iterations=2)
    # dilate = cv2.dilate(erode, None, iterations=2)
    mask_contours, contours, hierarchy = cv2.findContours(erodeGoal.copy(), cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

    if len(contours) > 0:
        # cv2.drawContours(frame, contours, -1, (255, 0, 0), 3)
        contoursMax = max(contours, key=cv2.contourArea)
        cv2.drawContours(frame, contoursMax, -1, (255, 105, 180), 3)
        M = cv2.moments(contoursMax)
        try:
            cx = int(M['m10'] / M['m00'])
            cy = int(M['m01'] / M['m00'])
            cv2.circle(frame, (int(cx), int(cy)), 2, (0, 0, 233), 5)
        except:
            print('OMG NO GOALS')


def findBalls(hsvBalls, lower, upper):
    mask = cv2.inRange(hsvBalls, lower, upper)
    global erodeBalls
    erodeBalls = cv2.erode(mask, None, iterations=2)
    # dilate = cv2.dilate(erode, None, iterations=2)
    mask_contours, contours, hierarchy = cv2.findContours(erodeBalls.copy(), cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    print(contours)

    if len(contours) > 00:
        # leian suurima kontuuri
        contoursMax = max(contours, key=cv2.contourArea)
        ((x, y), radius) = cv2.minEnclosingCircle(contoursMax)
        # circle: image, center, radius, color, thickness
        #print(x, y)
        calculateAngle(x, y)
        cv2.circle(frame, (int(x), int(y)), int(radius), (0, 255, 0), 2)
        cv2.circle(frame, (int(x), int(y)), 2, (0, 0, 255), 5)


def writefile(filename, hl, sl, vl, hu, su, vu):
    file = open(filename, 'w')
    file.write(str(hl) + "\n" + str(sl) + "\n" + str(vl) + "\n" +
               str(hu) + "\n" + str(su) + "\n" + str(vu) + "\n")
    file.close()


def readfile(filename):
    try:
        file = open(filename, 'r')
        hl = int(file.readline())
        sl = int(file.readline())
        vl = int(file.readline())
        hu = int(file.readline())
        su = int(file.readline())
        vu = int(file.readline())
        file.close()

        return hl, sl, vl, hu, su, vu

    except IOError:
        writefile(filename, 0, 0, 0, 0, 0, 0)
        return 0, 0, 0, 0, 0, 0


def trackbarWindow(windowName, value1, value2, value3, value4, value5, value6):
    cv2.namedWindow(windowName, flags=cv2.WINDOW_NORMAL)
    cv2.createTrackbar('Hlower', windowName, value1, 255, nothing)
    cv2.createTrackbar('Slower', windowName, value2, 255, nothing)
    cv2.createTrackbar('Vlower', windowName, value3, 255, nothing)
    cv2.createTrackbar('Hupper', windowName, value4, 255, nothing)
    cv2.createTrackbar('Supper', windowName, value5, 255, nothing)
    cv2.createTrackbar('Vupper', windowName, value6, 255, nothing)

def getTrackbars(windowName):
    hl = cv2.getTrackbarPos('Hlower', windowName)
    sl = cv2.getTrackbarPos('Slower', windowName)
    vl = cv2.getTrackbarPos('Vlower', windowName)
    hu = cv2.getTrackbarPos('Hupper', windowName)
    su = cv2.getTrackbarPos('Supper', windowName)
    vu = cv2.getTrackbarPos('Vupper', windowName)

    return hl, sl, vl, hu, su, vu


def click_and_crop(event, x, y, flags, param):
    # grab references to the global variables
    global refPt, cropping

    # if the left mouse button was clicked, record the starting
    # (x, y) coordinates and indicate that cropping is being
    # performed
    if event == cv2.EVENT_LBUTTONDOWN:
        refPt = [(x, y)]
        cropping = True

    # check to see if the left mouse button was released
    elif event == cv2.EVENT_LBUTTONUP:
        # record the ending (x, y) coordinates and indicate that
        # the cropping operation is finished
        refPt.append((x, y))
        cropping = False

        # draw a rectangle around the region of interest
        cv2.rectangle(hsv, refPt[0], refPt[1], (0, 255, 0), 2)
        #cv2.imshow("image333", hsv)


def nothing(x):
    pass


def autoCalib():
    global hsv

    while True:
        # display the image and wait for a keypress
        cv2.imshow("hsvclone", hsv)
        key = cv2.waitKey(1) & 0xFF

        # if the 'r' key is pressed, reset the cropping region
        if key == ord("r"):
            hsv = hsvclone.copy()

        # if the 'e' key is pressed, break from the loop
        elif key == ord("e"):
            break

    # if there are two reference points, then crop the region of interest
    # from teh image and display it
    if len(refPt) == 2:
        roi = hsvclone[refPt[0][1]:refPt[1][1], refPt[0][0]:refPt[1][0]]

    # find ROI height and width
    roi_height, roi_width = roi.shape[:2]

    H_values = []
    S_values = []
    V_values = []
    # get HSV values for each color and put them in arrays
    for i in range(0, roi_height):
        for j in range(0, roi_width):
            color = roi[i, j]
            H_values.append(color[0])
            S_values.append(color[1])
            V_values.append(color[2])

    # find min and max values from the arrays and save them to file
    Hmax = np.amax(H_values)
    Hmin = np.amin(H_values)
    Smax = np.amax(S_values)
    Smin = np.amin(S_values)
    Vmax = np.amax(V_values)
    Vmin = np.amin(V_values)
    cv2.destroyWindow("hsvclone")

    return Hmin, Smin, Vmin, Hmax, Smax, Vmax


###     MAIN PROGRAM    ###


###     READING FILES    ###
hlBalls, slBalls, vlBalls, huBalls, suBalls, vuBalls = readfile('ball_values.txt')
hlGoal, slGoal, vlGoal, huGoal, suGoal, vuGoal = readfile('goal_values.txt')


cap = cv2.VideoCapture(CAMERA)
_, erodeGoal = cap.read()  # v22rtustamise jaoks, (muutub findBalls ja findGoals funktsioonides)
erodeBalls = erodeGoal

BallTrackbarsExist = False
GoalTrackbarsExist = False

refPt = []


###    MAIN LOOP    ###
while (1):
    # frame buffer
    for i in range(BUFFER):
        ret, frame = cap.read()

    # Convert BGR to HSV
    hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
    hsv = cv2.GaussianBlur(hsv, (25, 25), 0)

    # get current positions of trackbars
    if BallTrackbarsExist:
        hlBalls, slBalls, vlBalls, huBalls, suBalls, vuBalls = getTrackbars('Balltrackbars')

    if GoalTrackbarsExist:
        hlGoal, slGoal, vlGoal, huGoal, suGoal, vuGoal = getTrackbars('Goaltrackbars')

    # define range of color in HSV
    lowerBalls = np.array([hlBalls, slBalls, vlBalls])
    upperBalls = np.array([huBalls, suBalls, vuBalls])

    lowerGoal = np.array([hlGoal, slGoal, vlGoal])
    upperGoal = np.array([huGoal, suGoal, vuGoal])

    findBalls(hsv, lowerBalls, upperBalls)
    findGoal(hsv, lowerGoal, upperGoal)

    cv2.imshow('frame', frame)
    cv2.imshow('hsv', hsv)
    cv2.imshow('blurredMaskGoal', erodeGoal)
    cv2.imshow('blurredMaskBalls', erodeBalls)

    # get the pressed key
    k = cv2.waitKey(5) & 0xFF

    # opening/closing of trackbar windows
    if k == ord('b'):
        if not BallTrackbarsExist:
            trackbarWindow('Balltrackbars', hlBalls, slBalls, vlBalls, huBalls, suBalls, vuBalls)
            BallTrackbarsExist = True

        else:
            cv2.destroyWindow('Balltrackbars')
            BallTrackbarsExist = False

    if k == ord('g'):
        if not GoalTrackbarsExist:
            trackbarWindow('Goaltrackbars', hlGoal, slGoal, vlGoal, huGoal, suGoal, vuGoal)
            GoalTrackbarsExist = True

        else:
            cv2.destroyWindow('Goaltrackbars')
            GoalTrackbarsExist = False

    # automated calibration. (trackbar windows must be closed in order to work)
    if k == ord('m'):
        hsvclone = hsv.copy()
        cv2.namedWindow("hsvclone")
        cv2.setMouseCallback("hsvclone", click_and_crop)
        hlBalls, slBalls, vlBalls, huBalls, suBalls, vuBalls = autoCalib()

    if k == ord('j'):
        hsvclone = hsv.copy()
        cv2.namedWindow("hsvclone")
        cv2.setMouseCallback("hsvclone", click_and_crop)
        hlGoal, slGoal, vlGoal, huGoal, suGoal, vuGoal = autoCalib()



    if k == 27:
        writefile('ball_values.txt', hlBalls, slBalls, vlBalls, huBalls, suBalls, vuBalls)
        writefile('goal_values.txt', hlGoal, slGoal, vlGoal, huGoal, suGoal, vuGoal)
        cap.release()
        break

cv2.destroyAllWindows()
