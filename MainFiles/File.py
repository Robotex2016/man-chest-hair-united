

def writefile(filename, hl, sl, vl, hu, su, vu):
    file = open(filename, 'w')
    file.write(str(hl) + "\n" + str(sl) + "\n" + str(vl) + "\n" +
               str(hu) + "\n" + str(su) + "\n" + str(vu) + "\n")
    file.close()

def readfile(filename):
    try:
        file = open(filename, 'r')
        hl = int(file.readline())
        sl = int(file.readline())
        vl = int(file.readline())
        hu = int(file.readline())
        su = int(file.readline())
        vu = int(file.readline())
        file.close()
        return hl, sl, vl, hu, su, vu

    except IOError:
        writefile(filename, 0, 0, 0, 0, 0, 0)
        return 0, 0, 0, 0, 0, 0