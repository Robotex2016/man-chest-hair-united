import cv2
import numpy as np
import math
import serial
import time
from time import sleep

import Vision as V
import Dribbler as D
import Motors as M
import Coilgun as C
import File


'''
# states[STATE]()
    states = {END: end,
              FIND_BALL: drive_to_ball,
              FIND_GOAL: find_goal,
              HIT_GOAL: hit_goalf
              }
'''



class Main:

    def __init__(self):
        print "INITIALIZING..."
        self.vision = V.Vision(hlBalls, slBalls, vlBalls, huBalls, suBalls, vuBalls, hlGoalb, slGoalb, vlGoalb, huGoalb, suGoalb, vuGoalb,
                               hlGoaly, slGoaly, vlGoaly, huGoaly, suGoaly, vuGoaly, hlLine, slLine, vlLine, huLine, suLine, vuLine,)
        self.dribler = D.Dribbler(port)
        self.motors = M.Motors(port)
        self.coilgun = C.Coilgun(port)
        print "ALL INITIALIZED"

    def __exit__(self):
        print "SHUTTING DOWN..."
        main.motors.shut_down()
        sleep(1)
        main.dribler.shut_down()
        sleep(1)
        main.coilgun.shut_down()
        main.vision.shut_down()
        sleep(0.3)
        port.close()
        print "SHUT DOWN"



if __name__ == '__main__':

    kohtunik = serial.Serial(
        port='COM5',            # make global
        baudrate=9600,
        stopbits=serial.STOPBITS_ONE,
        bytesize=serial.EIGHTBITS
    )
    kohtunik.isOpen()

    port = serial.Serial('COM4', baudrate=19200, timeout=None) # make global
    print("connected to: " + port.portstr)

    ###     OLEKUMASIN     ###
    END = 0
    FIND_BALL = 1
    FIND_GOAL = 2
    NO_BALLS = 3
    PREP_GOAL = 4
    DRIVE_AROUND = 5
    WAIT_REF = 6
    KATSE = 11


    ###     GLOBAL PARAMEETRID      ###
    hlBalls, slBalls, vlBalls, huBalls, suBalls, vuBalls = File.readfile('ball_values.txt')
    hlGoalb, slGoalb, vlGoalb, huGoalb, suGoalb, vuGoalb = File.readfile('goal_values_blue.txt')
    hlGoaly, slGoaly, vlGoaly, huGoaly, suGoaly, vuGoaly = File.readfile('goal_values_yellow.txt')
    hlLine, slLine, vlLine, huLine, suLine, vuLine = File.readfile('line_black.txt')
    _enemy_goal = 'b'   # 'b' - blue; 'y' - yellow
    _driveto_goal = 'b'
    _camera_buffer = 2
    _time_to_turn = 3
    _time_to_drive = 4
    Robotiv2ljak = 'A'
    Robotit2his = 'B'

    ###     ALGV22RTUSTAMINE     ###
    Ballx = 0.0
    Bally = 0.0
    Goalx = 0.0
    GOAL_checker = 1
    BALL_checker = 1
    t_start = time.time()
    t_end = time.time()


    ###     MAIN    ###
    main = Main()

    STATE = WAIT_REF
    #STATE = FIND_BALL
    #STATE = KATSE

    while True:
        main.vision.read_frame(_camera_buffer)

        if STATE != WAIT_REF:
            outr = ''
            while kohtunik.inWaiting() > 0:
                outr += kohtunik.read(1)

            if (outr != '') & (len(outr) >= 12):
                if (outr[0] == 'a') & (outr[1] == Robotiv2ljak):
                    if outr[2] == 'X':
                        if outr[3:7] == 'STOP':
                            main.motors.turn('stop')
                            port.write("<1wl0;2wl0;3wl0>\n")
                            STATE = WAIT_REF
                    elif outr[2] == Robotit2his:
                        if outr[3:7] == 'STOP':
                            main.motors.turn('stop')
                            port.write("<1wl0;2wl0;3wl0>\n")
                            STATE = WAIT_REF

        if STATE == WAIT_REF:
            print('WAIT_REF')
            out = ''
            while kohtunik.inWaiting() > 0:
                out += kohtunik.read(1)

            if (out != '') & (len(out) >= 12):
                if (out[0] == 'a') & (out[1] == Robotiv2ljak):
                    if out[2] == 'X':
                        if out[3:8] == 'START':
                            STATE = FIND_BALL
                            BALL_checker = 1
                            main.dribler.activate(True)

                    elif out[2] == Robotit2his:
                        if out[3:8] == 'START':
                            kohtunik.write('a' + Robotiv2ljak + Robotit2his + "ACK-------")
                            STATE = FIND_BALL
                            BALL_checker = 1
                            main.dribler.activate(True)
                        if out[3:7] == 'PING':
                            kohtunik.write('a' + Robotiv2ljak + Robotit2his + "ACK-------")

        elif STATE == FIND_BALL:
            print('FIND_BALL')
            if not main.dribler.ball_in():
                Ballx, Bally = main.vision.find_balls()
                '''if Ballx in range(310, 330) and Bally < 440:    # kas kasutame?
                    main.motors.drive_straight()
                    print("__DRIVE STRAIGHT")'''
                if Ballx == 0 and Bally == 0:
                    t_start = time.time()
                    STATE = NO_BALLS
                else:
                    if BALL_checker == 1:
                        BALL_checker = 2
                    else:
                        main.motors.drive_to_ball(Ballx, Bally)
            else:
                main.motors.turn('stop')
                STATE = FIND_GOAL
                GOAL_checker = 1
                port.write("<1wl45;2wl25;3wl-25>\n") # algul kiirus suureks
                #STATE = WAIT_REF

        elif STATE == FIND_GOAL:
            print('FIND_GOAL')
            if main.dribler.ball_in():
                Goalx, Goaly, goal_area = main.vision.find_goal(_enemy_goal)
                if main.motors.center_goal(Goalx):
                    if main.vision.checkpath():
                        if GOAL_checker == 1:
                            GOAL_checker = 2
                        else:
                            main.motors.turn('stop')
                            #STATE = WAIT_REF
                            main.coilgun.hit_goal()
                            STATE = FIND_BALL
                            BALL_checker = 1
                    else:
                        main.motors.move_sideways(False)
            else:
                STATE = FIND_BALL
                BALL_checker = 1
                print("_BALL LOST")

        elif STATE == NO_BALLS:
            print('NO BALLS')
            t_end = time.time()
            if (t_end - t_start) > _time_to_turn:
                STATE = PREP_GOAL
            else:
                Ballx, Bally = main.vision.find_balls()
                if Ballx == 0 and Bally == 0:
                    main.motors.turn('left')
                else:
                    STATE = FIND_BALL
                    BALL_checker = 1

        elif STATE == PREP_GOAL:
            print('PREP GOAL')
            Ballx, Bally = main.vision.find_balls()
            Goalx, Goaly, goal_area = main.vision.find_goal(_driveto_goal)

            if Ballx != 0 and Bally != 0:
                STATE = FIND_BALL
                BALL_checker = 1
            else:
                if Goalx == 0 or Goaly == 0:
                    main.motors.turn('f_right')
                else:
                    t_start = time.time()
                    STATE = DRIVE_AROUND

        elif STATE == DRIVE_AROUND:
            print('DRIVE AROUND')
            Ballx, Bally = main.vision.find_balls()
            Goalx, Goaly, goal_area = main.vision.find_goal(_driveto_goal)
            if goal_area > 20000:
                main.motors.turn('stop')
                if _driveto_goal == 'b':
                    _driveto_goal = 'y'
                else:
                    _driveto_goal = 'b'
                STATE = FIND_BALL               # v6ib panna PREP_GOAL
                BALL_checker = 1
            else:
                t_end = time.time()
                if (Ballx != 0 and Bally != 0) or (t_end - t_start) > _time_to_drive:
                    STATE = FIND_BALL
                    BALL_checker = 1
                else:
                    if Goalx != 0 and Goaly != 0:
                        main.motors.drive_to_ball(Goalx, Goaly) # tegelt drive to goal
                    else:
                        STATE = FIND_BALL
                        BALL_checker = 1


        elif STATE == KATSE:
            #print('KATSE')
            Ballx, Bally = main.vision.find_balls()
            #main.motors.turn('left')
            #Goalx = main.vision.find_goal()
            #main.motors.center_goal(Goalx)
            #main.motors.turn('left')
            #main.motors.center_goal(Goalx)


        main.vision.show_frame()


        k = cv2.waitKey(1) & 0xFF


        if k == ord('s'):
            if STATE == WAIT_REF:
                STATE = FIND_BALL
            else:
                main.motors.turn('stop')
                STATE = WAIT_REF

        if k == 27:
            main.__exit__()
            break
