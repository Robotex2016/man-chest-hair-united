
from time import sleep


class Dribbler:

    def __init__(self, port):
        self.port = port
        print "dribler initialized"

    def activate(self, on):
        if on:
            self.port.write("<dot58>\n")
            sleep(0.03)
        else:
            self.port.write("<df>\n")

    def ball_in(self):
        self.port.write("<bd>\n")
        #sleep(0.01)
        #n = '33'
        try:
            n = self.port.readline()
            if n is not None:
                if n[0] == '1':
                    return True
                else:
                    return False
        except Exception, e:
            print "Serial port error: " + str(e)
        sleep(0.01)

    def shut_down(self):
        self.port.write("<df>\n")
        sleep(0.1)
        print("dribler shut down")
