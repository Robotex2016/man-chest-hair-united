
from time import sleep
import cv2
import numpy as np


class Vision:

    def __init__(self, hlBalls, slBalls, vlBalls, huBalls, suBalls, vuBalls, hlGoalb, slGoalb, vlGoalb, huGoalb, suGoalb, vuGoalb,
                 hlGoaly, slGoaly, vlGoaly, huGoaly, suGoaly, vuGoaly, hlLine, slLine, vlLine, huLine, suLine, vuLine):
        self.cap = cv2.VideoCapture(0)
        sleep(0.3)
        self.frame = self.cap.read()
        sleep(0.3)
        self.frame = self.cap.read()
        sleep(0.1)
        self.hsv = self.frame
        self.lowerballs = np.array([hlBalls, slBalls, vlBalls])
        self.upperballs = np.array([huBalls, suBalls, vuBalls])
        self.lowergoalblue = np.array([hlGoalb, slGoalb, vlGoalb])
        self.uppergoalblue = np.array([huGoalb, suGoalb, vuGoalb])
        self.lowergoalyellow = np.array([hlGoaly, slGoaly, vlGoaly])
        self.uppergoalyellow = np.array([huGoaly, suGoaly, vuGoaly])
        self.lowerline = np.array([hlLine, slLine, vlLine])
        self.upperline = np.array([huLine, suLine, vuLine])
        #self.linemask = []
        print "camera initialized"

    def read_frame(self, buffer):
        for i in range(buffer):
            _, self.frame = self.cap.read()
        #print("frame olemas")
        self.hsv = cv2.cvtColor(self.frame, cv2.COLOR_BGR2HSV)
        self.hsv = cv2.GaussianBlur(self.hsv, (25, 25), 0)

    def show_frame(self):
        # frame 640x480
        cv2.imshow('frame', self.frame)


    def find_balls(self):
        mask = cv2.inRange(self.hsv, self.lowerballs, self.upperballs)
        #erodeBalls = cv2.erode(mask, None, iterations=2)
        erodeBalls = mask
        contours = cv2.findContours(erodeBalls.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)[-2]
        Ballx = 0
        Bally = 0

        if len(contours) > 0:
            contoursMax = max(contours, key=cv2.contourArea)
            ((Ballx, Bally), Ballradius) = cv2.minEnclosingCircle(contoursMax)
            # circle: image, center, radius, color, thickness
            #cv2.circle(self.frame, (int(Ballx), int(Bally)), int(Ballradius), (0, 255, 0), 2)
            cv2.circle(self.frame, (int(Ballx), int(Bally)), 2, (0, 0, 255), 5)
        if Ballx is None or Bally is None:
            return 0, 0
        else:
            if self.checkline(Bally):
                return Ballx, Bally
            else:
                return 0,0
            #return Ballx, Bally


    def find_goal(self, goal):
        if goal == 'b':
            mask = cv2.inRange(self.hsv, self.lowergoalblue, self.uppergoalblue)
        else:
            mask = cv2.inRange(self.hsv, self.lowergoalyellow, self.uppergoalyellow)
        #erodeGoal = cv2.erode(mask, None, iterations=2)
        erodeGoal = mask
        contours = cv2.findContours(erodeGoal.copy(), cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE) [-2]

        cx = 0
        cy = 0
        try:
            if len(contours) > 0:
                contoursMax = max(contours, key=cv2.contourArea)
                area = cv2.contourArea(contoursMax)
                if area > 500:
                    cv2.drawContours(self.frame, contoursMax, -1, (255, 105, 180), 3)
                    M = cv2.moments(contoursMax)
                    if M["m00"] != 0:
                        cx = int(M['m10'] / M['m00'])
                        cy = int(M['m01'] / M['m00'])
                        cv2.circle(self.frame, (int(cx), int(cy)), 2, (0, 0, 233), 5)
                return cx, cy, area
            else:
                return 0, 0, 0
        except:
            print "ERROR!!!!!!!!!!"
            return 0, 0, 0

    def checkline(self, Bally):
        if Bally < 400:
            crop_image = self.hsv[Bally:400, 140:500]
            mask = cv2.inRange(crop_image, self.lowerline, self.upperline)
            #erodeLine = cv2.erode(mask, None, iterations=2)
            erodeLine = mask
            contours = cv2.findContours(erodeLine.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)[-2]

            if len(contours) > 0:
                contoursMax = max(contours, key=cv2.contourArea)
                area = cv2.contourArea(contoursMax)
                if area > 50:
                    #print(area)
                    return False
            return True
        else:
            return True

    def checkpath(self):  # check path to goal
        crop_image = self.hsv[0:480, 260:380]
        mask = cv2.inRange(crop_image, self.lowerballs, self.upperballs)
        #erodeBalls = cv2.erode(mask, None, iterations=2)
        erodeBalls = mask
        contours = cv2.findContours(erodeBalls.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)[-2]

        if len(contours) > 0:
            contoursMax = max(contours, key=cv2.contourArea)
            area = cv2.contourArea(contoursMax)
            if area > 50:
                #print(area)
                return False
        return True



    def shut_down(self):
        self.cap.release()
        cv2.destroyAllWindows()
        print("vision shut down")

