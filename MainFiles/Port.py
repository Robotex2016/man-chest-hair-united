

class Port:

    def __init__(self, port):
        self.port = port
        self.m1p = 0
        self.m2p = 0
        self.m3p = 0

    def write_motors(self, m1, m2, m3):
        string = '1'
        if m1 != self.m1p:
            if m2 != self.m2p:
                if m3 != self.m3p:
                    string = "<1wl" + str(m1) + ";2wl" + str(m2) + ";3wl" + str(m3) + ">\n"
                    self.m1p = m1
                    self.m2p = m2
                    self.m3p = m3
                else:
                    string = "<1wl" + str(m1) + ";2wl" + str(m2) + ">\n"
                    self.m1p = m1
                    self.m2p = m2
            elif m3 != self.m3p:
                string = "<1wl" + str(m1) + ";3wl" + str(m3) + ">\n"
                self.m1p = m1
                self.m3p = m3
            else:
                string = "<1wl" + str(m1) + ">\n"
                self.m1p = m1
        elif m2 != self.m2p:
                if m3 != self.m3p:
                    string = "<2wl" + str(m2) + ";3wl" + str(m3) + ">\n"
                    self.m2p = m2
                    self.m3p = m3
                else:
                    string = "<2wl" + str(m2) + ">\n"
                    self.m2p = m2
        elif m3 != self.m3p:
            string = "<3wl" + str(m3) + ">\n"
            self.m3p = m3

        if string != '1':
            self.port.write(string)
