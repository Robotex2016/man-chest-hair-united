
import Port as P
from time import sleep
import numpy as np


class Motors:
    def __init__(self, port):
        self.port = port
        self.motors_port = P.Port(port)
        self.def_speed_max = 90  # 48 # 54 # 70
        self.max_speed_max = 100  # 60 # 62 # 76
        self.def_speed_min = 60
        self.max_speed_min = 66
        self.def_speed_vahe = self.def_speed_max - self.def_speed_min
        self.max_speed_vahe = self.max_speed_max - self.max_speed_min
        self.near = 200
        self.far = 150
        # wherever you are
        self.ala = self.near - self.far
        print "motors initialized"

    def drive_to_ball(self, ballx, bally):
        x = 320 - ballx  # vasakul positiivne
        y = 400 - bally  # 400 umbes palli koht dribbleris

        z = np.math.sqrt(x * x + y * y)

        suhe = y / z
        if x < 0:
            self.drive(-suhe, ballx, bally)
        else:
            self.drive(suhe, ballx, bally)

    def drive(self, suhe, ballx, bally):
        def_speed, max_speed = self.speed(bally)
        #def_speed = 64  # 48 # 54
        #max_speed = 66  # 60 # 62

        abs_angle = abs(suhe)

        speed_left = def_speed
        try:
            speed_right = int(speed_left / abs_angle)
        except:
            speed_right = speed_left

        # max v6imsuse kontroll
        if speed_right > max_speed:
            speed_left = (speed_left * max_speed) / speed_right
            speed_right = max_speed

        if suhe < 0:  # vahetan
            muutuja = speed_right
            speed_right = speed_left
            speed_left = muutuja

        # tagumise rattaga lisa p66ramine
        speed_back = 0
        if ballx < 260:
            speed_back = 28
        elif ballx > 380:
            speed_back = -28

        string = "<1wl" + str(speed_back) + ";2wl" + str(speed_right) + ";3wl" + str(speed_left) + ">\n"
        # print(string_left +"        "+ string_right)
        self.motors_port.write_motors(speed_back, speed_right, speed_left)
        sleep(0.02)

    def speed(self, bally):
        if bally < self.far:
            return self.def_speed_max, self.max_speed_max
        elif bally > self.near:
            return self.def_speed_min, self.max_speed_min
        else:
            x = self.near - bally
            kordaja = x / self.ala
            def_speed = self.def_speed_min + (self.def_speed_vahe * kordaja)
            max_speed = self.max_speed_min + (self.max_speed_vahe * kordaja)
            return int(def_speed), int(max_speed)

    # find and align with the goal
    def center_goal(self, Goalx):
            if Goalx == 0:
                self.motors_port.write_motors(60, 12, -12)
                return False
            elif Goalx > 350:
                self.motors_port.write_motors(-60, -12, 12)
                return False
            elif Goalx < 290:
                self.motors_port.write_motors(60, 12, -12)
                return False
            else:
                print("GOAL ALIGNED")
                self.turn('stop')
                return True
            sleep(0.02)

    def move_sideways(self, left):
        if left:
            self.motors_port.write_motors(-70, 35, -35)
        else:
            self.motors_port.write_motors(70, -35, 35)
        sleep(0.02)

    def turn(self, s):
        if s == 'right':
            self.motors_port.write_motors(-30, -30, 30)
        elif s == 'left':
            self.motors_port.write_motors(30, 30, -30)
        elif s == 'stop':
            self.motors_port.write_motors(0, 0, 0)
        elif s == 'f_right':
            self.motors_port.write_motors(-35, -35, 35)
        sleep(0.02)

    #hetkel ei kasuta
    def drive_straight(self):
        self.motors_port.write_motors(0, 54, 50) # mootorid liiguvad erinevalt 50 - 54

    def shut_down(self):
        self.port.write("<1wl0;2wl0;3wl0>\n")
        sleep(0.05)
        print("motors shut down")


