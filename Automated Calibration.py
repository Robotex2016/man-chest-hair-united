# import the necessary packages
import cv2
import numpy as np

# initialize the list of reference points and boolean indicating
# whether cropping is being performed or not
refPt = []
cropping = False
H_values = []
S_values = []
V_values = []
roi_height = 0
roi_width = 0
savefile = "ball_values.txt"


def click_and_crop(event, x, y, flags, param):
    # grab references to the global variables
    global refPt, cropping

    # if the left mouse button was clicked, record the starting
    # (x, y) coordinates and indicate that cropping is being
    # performed
    if event == cv2.EVENT_LBUTTONDOWN:
        refPt = [(x, y)]
        cropping = True

    # check to see if the left mouse button was released
    elif event == cv2.EVENT_LBUTTONUP:
        # record the ending (x, y) coordinates and indicate that
        # the cropping operation is finished
        refPt.append((x, y))
        cropping = False

        # draw a rectangle around the region of interest
        cv2.rectangle(image, refPt[0], refPt[1], (0, 255, 0), 2)
        cv2.imshow("image", image)


cap = cv2.VideoCapture(0)

while True:
    _, image = cap.read()
    image = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)
    image = cv2.GaussianBlur(image, (25, 25), 0)
    cv2.imshow("image", image)
    key = cv2.waitKey(1) & 0xFF

    # if i key is pressed, video feed is stopped to last frame
    if key == ord("i"):
        break

clone = image.copy()
cv2.namedWindow("image")
cv2.setMouseCallback("image", click_and_crop)

# keep looping until the 'q' key is pressed
while True:
    # display the image and wait for a keypress
    cv2.imshow("image", image)
    key = cv2.waitKey(1) & 0xFF

    # if the 'r' key is pressed, reset the cropping region
    if key == ord("r"):
        image = clone.copy()

    # if the 'c' key is pressed, break from the loop
    elif key == ord("c"):
        break

# if there are two reference points, then crop the region of interest
# from teh image and display it
if len(refPt) == 2:
    roi = clone[refPt[0][1]:refPt[1][1], refPt[0][0]:refPt[1][0]]

# find ROI height and width
roi_height, roi_width = roi.shape[:2]

# get HSV values for each color and put them in arrays
for i in range(0, roi_height):
    for j in range(0, roi_width):
        color = roi[i, j]
        H_values.append(color[0])
        S_values.append(color[1])
        V_values.append(color[2])

# find min and max values from the arrays and save them to file
Hmax = np.amax(H_values)
Hmin = np.amin(H_values)
Smax = np.amax(S_values)
Smin = np.amin(S_values)
Vmax = np.amax(V_values)
Vmin = np.amin(V_values)

file = open(savefile, 'w+')
file.write(str(Hmin) + "\n" + str(Smin) + "\n" + str(Vmin) + "\n" +
           str(Hmax) + "\n" + str(Smax) + "\n" + str(Vmax) + "\n")
file.close()

# close all open windows
cv2.destroyAllWindows()
