import cv2
import numpy as np

import serial
from time import sleep

###     FUNCTIONS     ###

def findGoal(hsvGoal, lower, upper):
    mask = cv2.inRange(hsvGoal, lower, upper)
    global erodeGoal
    erodeGoal = cv2.erode(mask, None, iterations=2)
    # dilate = cv2.dilate(erode, None, iterations=2)
    mask_contours, contours, hierarchy = cv2.findContours(erodeGoal.copy(), cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

    if len(contours) > 0:
        # cv2.drawContours(frame, contours, -1, (255, 0, 0), 3)
        contoursMax = max(contours, key=cv2.contourArea)
        cv2.drawContours(frame, contoursMax, -1, (255, 105, 180), 3)
        M = cv2.moments(contoursMax)
        try:
            cx = int(M['m10'] / M['m00'])
            cy = int(M['m01'] / M['m00'])
            cv2.circle(frame, (int(cx), int(cy)), 2, (0, 0, 233), 5)
        except:
            print('OMG NO GOALS')


def findBalls(hsvBalls, lower, upper):
    mask = cv2.inRange(hsvBalls, lower, upper)
    global erodeBalls, Ballx, Bally, Ballradius
    erodeBalls = cv2.erode(mask, None, iterations=2)
    # dilate = cv2.dilate(erode, None, iterations=2)
    mask_contours, contours, hierarchy = cv2.findContours(erodeBalls.copy(), cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

    if len(contours) > 0:
        # leian suurima kontuuri
        contoursMax = max(contours, key=cv2.contourArea)
        ((Ballx, Bally), Ballradius) = cv2.minEnclosingCircle(contoursMax)
        # circle: image, center, radius, color, thickness
        cv2.circle(frame, (int(Ballx), int(Bally)), int(Ballradius), (0, 255, 0), 2)
        cv2.circle(frame, (int(Ballx), int(Bally)), 2, (0, 0, 255), 5)


def writefile(filename, hl, sl, vl, hu, su, vu):
    file = open(filename, 'w')
    file.write(str(hl) + "\n" + str(sl) + "\n" + str(vl) + "\n" +
               str(hu) + "\n" + str(su) + "\n" + str(vu) + "\n")
    file.close()


def readfile(filename):
    try:
        file = open(filename, 'r')
        hl = int(file.readline())
        sl = int(file.readline())
        vl = int(file.readline())
        hu = int(file.readline())
        su = int(file.readline())
        vu = int(file.readline())
        file.close()

        return hl, sl, vl, hu, su, vu

    except IOError:
        writefile(filename, 0, 0, 0, 0, 0, 0)
        return 0, 0, 0, 0, 0, 0






###     MAIN PROGRAM    ###


###     DECLARATION, GLOBAL VARIABLES    ###
hlBalls, slBalls, vlBalls, huBalls, suBalls, vuBalls = readfile('ball_values.txt')
hlGoal, slGoal, vlGoal, huGoal, suGoal, vuGoal = readfile('goal_values.txt')


cap = cv2.VideoCapture(1)
_, erodeGoal = cap.read()  # v22rtustamise jaoks, (muutub findBalls ja findGoals funktsioonides)
erodeBalls = erodeGoal

Ballx = 0.0
Bally = 0.0
Ballradius = 0.0

port = serial.Serial('COM10', baudrate=9600, timeout=None)


###    MAIN LOOP    ###

while (1):
    # Take each frame
    _, frame = cap.read()

    # Convert BGR to HSV
    hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
    hsv = cv2.GaussianBlur(hsv, (25, 25), 0)

    # define range of color in HSV
    lowerBalls = np.array([hlBalls, slBalls, vlBalls])
    upperBalls = np.array([huBalls, suBalls, vuBalls])

    lowerGoal = np.array([hlGoal, slGoal, vlGoal])
    upperGoal = np.array([huGoal, suGoal, vuGoal])

    findBalls(hsv, lowerBalls, upperBalls)
    #findGoal(hsv, lowerGoal, upperGoal)

    cv2.imshow('frame', frame)

    #frame 640x480

    #print(Ballradius)
    #print(Ballx, Bally)
    if (Ballradius > 20):
        if Ballx > 350:
            print("right")
            # p66ra paremale
            pin = "1wl40\n"
        elif Ballx < 290:
            # p66ra vasakule
            print("left")
            pin = "1wl-40\n"
        else:
            # kohal
            print("WOHO")
            pin = "1wl0\n"

    else:
        # palli pole, p66ra vasakule
        print("otsin palli")
        pin = "1wl0\n"

    port.write(pin)
    sleep(0.1)

    # get the pressed key
    k = cv2.waitKey(5) & 0xFF

    if k == 27:
        cap.release()
        break



cv2.destroyAllWindows()